/*

	TheThings.io Node-RED Node
	Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>
	This project is licensed under the terms of the MIT license.

*/

module.exports = function(RED) {

	function getClient(token, secure) {
		config = {'thingToken': token};
		return require('thethingsio-api/http-client')(config, secure);
	}

	function sendData(node, token, secure, key, value, datetime){

		var client = getClient(token, false);
		var data = {'values': [
			{'key': key, 'value': value, 'datetime': datetime}
		]};

		client.thingWrite(data, function(error, data) {
			// this and any other error will be handled in the domain upstream
			if (error) throw error;
			node.status({fill: "green", shape:"dot", text: "Status OK"});
		});

	}

	function thethingsioNode(n) {

		RED.nodes.createNode(this, n);

		this.on("input", function(msg) {

			var node = this;
			var now = new Date();
			var token = msg.token || n.token;
			var key = msg.key || n.key;
			var value = msg.payload;
			var datetime = msg.timestamp || now.toISOString().replace(/[-:TZ\.]/g,'');

			var domain = require('domain').create()
			domain.on('error', function(error) {
				node.error(error, msg);
				node.status({fill: "red", shape:"dot", text: "Status ERROR"});
			});

			domain.run(function() {
				sendData(node, token, n.secure, key, value, datetime);
			});

		});
	}

	RED.nodes.registerType("thethingsio",thethingsioNode);

}
