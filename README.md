# node-red-contrib-thethingsio

Node-RED node to send data to [thethings.io][1] IoT platform

## Install it

Local user installation:

```
cd ~\.node-red
npm install node-red-contrib-thethingsio
```

Global installation (as root):

```
npm install -g node-red-contrib-thethingsio
```

Restart your node-red instance and you should have a "thethingsio" output node available.

## Use it

This node sends the payload of the message (<code>msg.payload</code>) to your [thethings.io][1] account.

<ul>
   <li><strong>Token</strong>: Token which is used to authenticate to a thing.</li>
   <li><strong>Secure</strong>: Whether it should use a secure connection (HTTPS) or not.</li>
   <li><strong>Key</strong>: Name of the variable to send the data to.</li>
   <li><strong>Name</strong>: Name of the node.</li>
</ul>

You can override the token and key properties with the <code>msg.token</code> and <code>msg.key</code> message attributes. Likewise you can set a custom <code>msg.timestamp</code> to be used (format must be YYYYMMDDhhmmss).

[1]: http://thethings.io
